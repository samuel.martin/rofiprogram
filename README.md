# RofiPrograms

This repository provides a python module to easily create small programs using
[Rofi](https://github.com/davatorium/rofi). For more details see the `src/rofi_program` folder. This repository also
contains some useful wrappers for different Unix commands/program (`xrandr`, `polybar`, `bspwm`, ...), for more details
see the `src/ nix_utils` folder. The intention is to create some small programs to execute simple tasks e.g. configure a
monitor layout, restart the pc etc. Some example programs can be found in the `src/programs` folder Currently the repo
contains the following programs:

- `example_program.py`: An example program demonstrating the use of the `rofie_program` module
- `monitor_control.py`: Easily configure multiple monitors including the restart of polybar for each monitor
- `bluetooth_control.py`: List bluetooth devices to connect to

These can are listed and can be run when executing `src/run.py`. Each of these programs is a child of the `Program`
class which can be executed by calling the `run` function. See `src/run.py` for an example. Alternatively
`src/run_json.py` can be used to run a program configured via a json file. For more details see the `JsonProgram` class
in `src/programs/json_program.py`. An example program configured in such a way can be found in
`configs/json_program_example.json`.
