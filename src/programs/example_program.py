from rofi_program.item import RPCommand
from rofi_program.menu import RPMenu, RPMenuEntry
from programs import Program


class ExampleProgram(Program):

    def __init__(self):
        options = [
            RPMenuEntry('Test 1', 'a', RPCommand(['notify-send', '1'])),
            RPMenuEntry('Test 2', 'b', RPCommand(['notify-send', '2']))
        ]
        menu1 = RPMenu(options)
        options2 = [
            RPMenuEntry('Leaf', 'a', RPCommand(['notify-send', 'leaf'])),
            RPMenuEntry('Submenu', 'b', menu1)
        ]
        self.menu = RPMenu(options2)
