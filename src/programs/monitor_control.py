from rofi_program.item import RPFunction
from rofi_program.menu import RPMenuEntry, RPMenu, MenuOrientation

from unix_utils.xrandr import getConnectedOutputs
from unix_utils.monitor import Monitor
import unix_utils.monitor_commands as mtr
import unix_utils.xrandr as xrandr
import unix_utils.polybar as polybar
from programs import Program


def customOutputSettings(monitors: dict[Monitor]):
    """
    Applies custom settings to the given dictionary of monitors
    """
    if "eDP-1" in monitors:
        monitors["eDP-1"].mode = "2560x1440"


def getLayoutOptions(outputs: dict[Monitor]) -> list[RPMenuEntry]:
    """
    Returns the list of menu entries listing the layout options
    """
    optionsChooseOne = []
    for index, output in enumerate(outputs):
        optionsChooseOne.append(RPMenuEntry(output, str(index), RPFunction(mtr.onlyOneMonitor, outputs, output)))

    optionsLayout = [
        RPMenuEntry('Only One Dipslay (o)', 'o', RPMenu(optionsChooseOne)),
        RPMenuEntry('Horizontal (b)', 'b', RPFunction(mtr.allLine, outputs, mtr.Direction.Left)),
        RPMenuEntry('Vertical (v)', 'v', RPFunction(mtr.allLine, outputs, mtr.Direction.Up))
    ]

    return optionsLayout


def getSingleMonitorMenu(outputs: dict[Monitor]) -> list[RPMenuEntry]:
    """
    Returns the list of menu entries listing actions for one single monitor, for each monitor
    """
    options = []

    def applyResolution(resolution: str, monitor: Monitor):
        monitor.mode = resolution
        xrandr.runCommands({'0': monitor})
        polybar.startPolybars(outputs)

    for index, output in enumerate(outputs):
        monitor = outputs[output]
        resolutions = xrandr.getAvailableMonitorModes(output)
        resolutionEntries = [RPMenuEntry(resolution, None, RPFunction(applyResolution, resolution, monitor))
                             for resolution in resolutions]

        monitorOptions = [
            RPMenuEntry('Change Resolution (r)', 'r', RPMenu(resolutionEntries)),
            RPMenuEntry('Turn On (o)', 'o', RPFunction(mtr.turnMonitorOn, monitor)),
            RPMenuEntry('Turn Off (f)', 'f', RPFunction(mtr.turnMonitorOff, monitor))
        ]

        options.append(RPMenuEntry(f'{output} ({index})', str(index), RPMenu(monitorOptions)))

    return options


class MonitorControl(Program):

    def __init__(self):
        mtr.removeDisconnectedMonitors()
        outputs = getConnectedOutputs()
        customOutputSettings(outputs)

        print("Found the following ouptputs")
        for out in outputs:
            print(outputs[out])

        options = []

        options.append(RPMenuEntry('Restart Polybar (p)', 'p', RPFunction(polybar.startPolybars, outputs)))
        options.append(RPMenuEntry('Single Monitor Action (s)', 's', RPMenu(getSingleMonitorMenu(outputs))))
        if len(outputs) > 1:
            options.append(RPMenuEntry('Layout Monitors (m)', 'm', RPMenu(getLayoutOptions(outputs))))

        self.menu = RPMenu(options, MenuOrientation.Vertical)
