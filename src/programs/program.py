from rofi_program.menu import RPMenu


class Program:
    """Base class for all programs. Overwrite init to create the wanted menu"""
    menu: RPMenu

    def __init__(self):
        self.menu = RPMenu([])

    def run(self):
        self.menu.run()
