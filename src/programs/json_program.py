import json

from rofi_program.item import RPCommand
from rofi_program.menu import RPMenu, RPMenuEntry
from programs import Program


class JsonProgram(Program):

    """
    Constructor. It reads the program structure from a given json file. The json file needs to have the following
    structure:
        - One "options" entry which contains a list with options, each option must have the following attributes:
            - "type", its value must either be "menu" or "command".
            - "name": The name of this entry
            - "key": The key to press to directly select this entry
            - Additional attributes as described below depending on the type

    A "menu" must contain the additional attribute "options" with the contained
    entries, this allows for nested menus.

    A "command" type must contain the additional attribute "command" which is another object with at least the entry
    "args" which is a list of strings. This list is passed to the RPCommand constructor and must represent the command
    to execute. Additional it can contain any additional entries which are passed through to the underlying
    subprocess.run can and thus must be valid arguments to it.

    An example json config can be found in configs/json_program_example.json

    :param config_file: Path to the json config file
    """
    def __init__(self, config_file: str):
        with open(config_file, 'r') as file:
            config = json.load(file)
            self.menu = RPMenu(self.parse_menu(config))

    def parse_menu(self, config: dict) -> list[RPMenuEntry]:
        options = []
        for entry in config['options']:
            if entry['type'] == 'menu':
                options.append(RPMenuEntry(entry['name'], entry['key'], RPMenu(self.parse_menu(entry))))
            elif entry['type'] == 'command':
                other_args = entry['command']
                args = other_args.pop('args')
                options.append(RPMenuEntry(entry['name'], entry['key'], RPCommand(args, args=other_args)))

        return options
