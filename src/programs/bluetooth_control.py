from rofi_program.item import RPFunction, RPAction
from rofi_program.menu import RPMenuEntry, RPMenu, MenuOrientation
from unix_utils.bluetooth import getDevices, BluetoothDevice
from programs import Program


class BluetoothControl(Program):
    def __init__(self):
        devices = getDevices()
        # TODO: Find better solution for key
        connect_menu = [RPMenuEntry(device.name, None, RPAction(device.connect)) for device in devices]
        options = [
            RPMenuEntry('Connect Device (c)', 'c', RPMenu(connect_menu)),
                ]

        self.menu = RPMenu(options)
