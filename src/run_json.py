import sys
from programs import JsonProgram


def main():
    JsonProgram(sys.argv[1]).run()


if __name__ == '__main__':
    main()
