from typing import List, Optional


class Monitor:
    """
    Class which represents one (connected) monitor as outputed by xrandr
    """
    name: str
    """ The name used by xrandr e.g. eDP-1 """
    positionCommands: List
    """ A list of commands describing the position e.g. ['--right-of', 'DP-1'] """
    mode: Optional[str]
    """ The mode/resolution of the monitor, e.g. '2560x1440' """
    active: bool
    """ If the monitor schould be used / be active """
    isPrimary: bool
    """ If the monitor is the primary monitor """

    def __init__(self, name: str, isPrimary: bool = False):
        self.name: str = name
        self.positionCommands = []
        self.mode: str = None
        self.active: bool = True
        self.isPrimary: bool = isPrimary

    def getXrandrCommand(self) -> List[str]:
        """
        Returns a list of commands as used by subprocess.run which are the necessary xrandr flags for this monitor.
        e.g. ['--output', 'eDP-1', '--mode', '2560x1440']
        """
        command = ['--output', self.name]
        if self.active:
            command += self.positionCommands
            if self.mode is not None:
                command += ['--mode', self.mode]
            else:
                command += ['--auto']
        else:
            command += ['--off']

        # we currently don't want to add that, is it will change the primary status of all monitors
        # command += ['--primary'] if self.isPrimary else ['--noprimary']
        return command

    def __str__(self):
        return f"{self.name} size: {self.mode} active: {self.active} primay: {self.isPrimary}"
