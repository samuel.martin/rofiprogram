import subprocess
import sys
import re


class Network:
    essid: str
    signalStrength: int = 0
    rate: int = 0

    def __init__(self, essid):
        self.essid = essid

    def addRate(self, rate):
        self.rate = max(rate, self.rate)

    def addSignalStrength(self, strength):
        self.signalStrength = max(strength, self.signalStrength)

    def connect(self):
        subprocess.run(['nmcli', 'connection', 'up', f"\'{self.essid}\'"])

    def __str__(self):
        return f"'{self.essid}' at {self.signalStrength}% with {self.rate} Mbit/s"


def getAvailableWifiNetworks() -> dict[Network]:
    """Returns a dictionary of available networs. Key is the network essid, value the network instance"""
    networks = {}
    for entry in subprocess.check_output(['nmcli', '--terse', 'device', 'wifi', 'list'])\
            .decode(sys.stdout.encoding).split('\n'):
        data = re.split(r'(?<!\\):', entry)[1:]
        if len(data) >= 6:
            rate = int(data[4].split(' ')[0])
            if data[4].split(' ')[1] == 'Kbit/s':
                rate /= 1000
            elif data[4].split(' ')[1] == 'Gbit/s':
                rate *= 1000
            name = data[1]
            networks[name] = Network(name)
            networks[name].addSignalStrength(int(data[5]))
            networks[name].addRate(rate)

    for name, network in networks.items():
        print(network)


