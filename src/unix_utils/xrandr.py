import subprocess
from typing import Dict, List
import sys

from unix_utils.monitor import Monitor


def getConnectedOutputs() -> Dict[str, Monitor]:
    """
    Returns a dictionary of names of connected outputs
    :return: Dictionary of connected monitors where the key is the name
    """
    # Get Connected outputs
    xrandr_cmd = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE)
    awk_cmd = subprocess.check_output(['awk', '/ connected/ {print $1}'], stdin=xrandr_cmd.stdout)
    xrandr_cmd.wait()
    outputs = awk_cmd.decode(sys.stdout.encoding).split('\n')
    monitors = {}
    # last output is empty as there is a newline at the end
    for output in outputs[:-1]:
        monitors[output] = Monitor(output)

    # Get primary output
    xrandr_cmd = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE)
    awk_cmd = subprocess.check_output(['awk', '/primary/ {print $1}'], stdin=xrandr_cmd.stdout)
    xrandr_cmd.wait()
    outputs = awk_cmd.decode(sys.stdout.encoding).split('\n')
    for output in outputs[:-1]:
        monitors[output].isPrimary = True

    return monitors


def getZombieOutputs() -> Dict[str, Monitor]:
    """
    Returns xrandr outputs which are disconnected but have a resolution configured
    :return: Dictionary where the name is the key. All monitors have active set to False
    """
    xrandr_cmd = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE)
    awk_cmd = subprocess.check_output(['awk', '/ disconnected [0-9]*x[0-9]*/ {print $1}'], stdin=xrandr_cmd.stdout)
    xrandr_cmd.wait()
    outputs = awk_cmd.decode(sys.stdout.encoding).split('\n')
    monitors = {}
    # last output is empty as there is a newline at the end
    for output in outputs[:-1]:
        monitors[output] = Monitor(output)
        monitors[output].active = False

    return monitors


def findPrimary(monitors: Dict[str, Monitor]) -> Monitor:
    """
    Searches in the given dictionary of monitors for the primary. Expects to find only one
    :param monitors: The dictionary of monitors
    :return: The first primary monitor found, if no was found returns the any monitor in the dictionary
    """
    for _, monitor in monitors.items():
        if monitor.isPrimary:
            return monitor
    return next(iter(monitors.values()))


def runCommands(monitors: Dict[str, Monitor]):
    """
    Runs the xrandr command for the given monitors
    :param monitors: Dictionary of monitors, the key is the monitor name
    """
    commands = ['xrandr']
    for _, monitor in monitors.items():
        commands += monitor.getXrandrCommand()

    print(f'Run command: {commands}')
    subprocess.run(commands)

def getAvailableMonitorModes(monitor: Monitor) -> List[str]:
    #  TODO: Currently only works of we have only one monitor
    """
    Returns the available modes/resolutions for a given monitor
    Returns an array of strings, one string for each mode. The string is in the format as the xrandr --mode flag
    accepts it
    :param monitor: The monitor we want to get the resolution of
    """
    xrandr_cmd = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE)
    awk_cmd = subprocess.check_output(['awk', '/ [0-9]{3,4}x[0-9]{3,4} *[0-9]{2}/ { print $1;}'], stdin=xrandr_cmd.stdout)
    xrandr_cmd.wait()
    outputs = awk_cmd.decode(sys.stdout.encoding).split('\n')
    return outputs[:-1]


