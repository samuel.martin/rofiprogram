import subprocess
from typing import Dict

from unix_utils.monitor import Monitor


def startPolybars(monitors: Dict[str, Monitor]):
    """
    Kills all active polybar instances and restarts them for each connected monitor
    :param monitors: A dictionary with the connected monitors
    """
    subprocess.run(['killall', 'polybar'])
    for name in monitors:
        if (monitors[name].active):
            if (monitors[name].isPrimary):
                print(f'sh /home/samuel/.config/bspwm/scripts/start-polybar.sh {name} primary')
                subprocess.run(['sh', '/home/samuel/.config/bspwm/scripts/start-polybar.sh', name, 'primary'])
            else:
                subprocess.run(['sh', '/home/samuel/.config/bspwm/scripts/start-polybar.sh', name, 'secondary'])
                print(f'sh /home/samuel/.config/bspwm/scripts/start-polybar.sh {name} secondary')
