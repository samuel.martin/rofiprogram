from .monitor import Monitor


class Desktop:
    name: str
    monitor: Monitor
    temporary: bool

    def __init__(self, name, monitor):
        self.name = name
        self.monitor = monitor
        self.temporary = name.startswith('tmp_')

    def __str__(self):
        return f'{self.name} on {self.monitor.name}'
