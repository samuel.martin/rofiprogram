import subprocess


class BluetoothDevice:
    id: str
    name: str

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def connect(self):
        """ Connect to this device """
        subprocess.run(['bluetoothctl', 'connect', self.id])

    def __str__(self) -> str:
        return f"{self.name} ({self.id})"

    def __repr__(self) -> str:
        return f"{self}"


def getDevices() -> list[BluetoothDevice]:
    devices_raw = subprocess.check_output(['bluetoothctl', 'devices']).decode('UTF-8')
    devices = []
    for device_raw in devices_raw.split("\n"):
        values = device_raw.split(" ")
        if (len(values) >= 3):
            devices.append(BluetoothDevice(values[1], ' '.join(values[2:])))

    return devices
