from enum import Enum
from typing import Dict

import unix_utils.xrandr as xrandr
import unix_utils.bspwm as bspwm
import unix_utils.polybar as polybar
from unix_utils.monitor import Monitor


class Direction(Enum):
    Left = '--left-of'
    Right = '--right-of'
    Up = '--above'
    Down = '--below'


def runCommands(monitors: Dict[str, Monitor]):
    """
    Runs the xrandr and polybar commands for the given monitors
    """
    print("Monitor configuration")
    for mon in monitors:
        print(monitors[mon])
        print(monitors[mon].getXrandrCommand())
    xrandr.runCommands(monitors)
    polybar.startPolybars(monitors)


def removeDisconnectedMonitors():
    """
    Removes "zombie" outputs by turning them off on xrandr, move away their BSPWM desktops and restart polybars
    A "zombie" output is one which has a xrandr configuration but is disconnected
    """
    zombieOutputs = xrandr.getZombieOutputs()
    connectedOutputs = xrandr.getConnectedOutputs()
    primary = xrandr.findPrimary(connectedOutputs)
    for _, monitor in zombieOutputs.items():
        print(f'Remove zombie output {monitor.name}')
        bspwm.removeMonitor(monitor, primary)

    # Only do something if we had zombie outputs
    if len(zombieOutputs) > 0:
        xrandr.runCommands(zombieOutputs)
        polybar.startPolybars(connectedOutputs)


def onlyOneMonitor(monitors: dict[str, Monitor], name: str):
    """
    Turns off all monitor except the given one.
    Doesn't run the commands if the given monitor could not be found.
    :param name: The name of the monitor to keep turned on
    """
    foundMonitor = False
    for _, monitor in monitors.items():
        if not monitor.name == name:
            monitor.active = False
            monitor.isPrimary = False
        else:
            monitor.active = True
            monitor.isPrimary = True
            foundMonitor = True

    if foundMonitor:
        runCommands(monitors)


def onlyPrimaryMonitor(monitors: dict[str, Monitor]):
    """
    Turns of all connected monitors except the primary
    """
    primary = xrandr.findPrimary(monitors)
    for _, monitor in monitors.items():
        if not monitor.isPrimary:
            monitor.active = False
            bspwm.removeMonitor(monitor, primary)

    runCommands(monitors)


def allLine(monitors: dict[str, Monitor], direction: Enum = Direction.Left):
    """
    Puts all monitors in a vertical line. The primary is the first, the rest in arbitrary order, in the given direction
    :param direction: The direction of the line
    """
    primary = xrandr.findPrimary(monitors)
    for _, monitor in monitors.items():
        if not monitor.isPrimary:
            monitor.positionCommands = [direction.value, primary.name]

    runCommands(monitors)


def turnMonitorOn(monitor: Monitor):
    """
    Turns on the given monitor
    :param monitor: The given monitor
    """
    monitor.active = True
    xrandr.runCommands({'0': monitor})


def turnMonitorOff(monitor: Monitor):
    """
    Turns off the given monitor
    :param monitor: The given monitor
    """
    monitor.active = False
    xrandr.runCommands({'0': monitor})
