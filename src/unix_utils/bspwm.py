import socket
from typing import Dict
from unix_utils.desktop import Desktop
from unix_utils.monitor import Monitor


def getDesktopsFromMonitor(monitor: Monitor) -> Dict[str, Desktop]:
    """
    Retreives all BSPWM desktops which are on the given monitor
    :param monitor: The monitor we want to get the desktops from
    :return: Dictionary with the desktops, key is the name of the desktop
    """
    desktops = {}
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
        s.connect('/tmp/bspwm_0_0-socket')
        command = b'query\0-D\0--names\0--monitor\0' + bytes(monitor.name, 'UTF-8') + b'\0'
        s.send(command)
        data = s.recv(1024).decode('UTF-8')
        desktop_names = data.split('\n')
        # make sure that the response does not start with query indicating that something was wrong with the sent
        # command
        if not data[1:6] == 'query':
            for name in desktop_names:
                if not name == '':
                    desktops[name] = Desktop(name, monitor)

    return desktops


def moveDesktopToMonitor(desktop: Desktop, monitor: Monitor):
    """
    Moves the given desktop to the given monitor
    :param desktop: The given desktop
    :param monitor: The given monitor
    """
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
        s.connect('/tmp/bspwm_0_0-socket')
        command = b'desktop\0' + bytes(desktop.name, 'UTF_8') + b'\0--to-monitor\0' + bytes(monitor.name, 'UTF_8') + b'\0'
        s.send(command)


def removeMonitor(monitor: Monitor, refugeMonitor: Monitor):
    """
    Removes the given monitor from BSPWM, moves all non temporary desktops to refugeMonitor
    :param monitor: Monitor to remove
    :param refugeMonitor: Monitor to move desktops to
    """
    desktops = getDesktopsFromMonitor(monitor)
    for _, desktop in desktops.items():
        if not desktop.temporary:
            moveDesktopToMonitor(desktop, refugeMonitor)

    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as s:
        s.connect('/tmp/bspwm_0_0-socket')
        command = b'monitor\0' + bytes(monitor.name, 'UTF_8') + b'\0--remove\0'
        s.send(command)


def getDesktops(monitors: Dict[str, Monitor]) -> Dict[str, Desktop]:
    """
    Get all BSPWM Desktops
    :param monitors: The dictionary of monitors to check for desktops
    :return: Dictionary of with the desktops, the key is the name of the desktop
    """
    desktops = {}
    for _, monitor in monitors.items():
        desktops.update(getDesktopsFromMonitor(monitor))

    return desktops
