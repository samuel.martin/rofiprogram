# RofiProgram (RP)

This folder provides the neccessary class to easily construct a program using
[Rofi](https://github.com/davatorium/rofi). The program can display a list of options which either performs some actions
when choosen or lead to a submenu.

