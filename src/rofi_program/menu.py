from subprocess import run, Popen, PIPE
from typing import List, Optional
import sys
from enum import Enum
from os.path import dirname, join
from os import pardir

from .item import RPItem


CONFIG_DIR = join(dirname(__file__), pardir, pardir, 'configs/rofi')


class RPMenuEntry:
    """
    A entry in `RPMenu`. Contains the next `RPItem` which is used when this entry gets selected

    One entry defines:
    - name
    - keypress to activate
    - what happens next
        - is either a command
        - or a next menu
    """
    name: str
    """ The name of the entry. This gets displayed to the user """
    keypress: Optional[str]
    """ The keypress, as used in the rofi config, which directly activates this entry """
    next_item: RPItem
    """ The item selected with this entry """

    def __init__(self, name, keypress, next_item):
        self.name = name
        self.keypress = keypress
        self.next_item = next_item

    def get_option_text(self) -> str:
        """ Returns the text describing this entry """
        return self.name

    def get_parameter(self, index) -> List[Optional[str]]:
        """
        Returns a list of arguments added to rofi to define that the custom key press of the given index is the
        keys defined under `self.keypress`
        :param index: The index of the custom key press
        :return: List of arguments as used by `subprocess.run`
        """
        if self.keypress is None:
            return []
        else:
            return [f'-kb-custom-{index+1}', self.keypress]


class MenuOrientation(Enum):
    """ Enum for the menu orientation """
    Horizontal = 1,
    Vertical = 2


class RPMenu(RPItem):
    """
    A Node representing a menu. The menu consist of a list of `RPMenuEntry`. When run it will let the user chose one
    of the menu items.
    """
    options: List[RPMenuEntry]
    rofi_command: List[str]

    def __init__(self, options: List[RPMenuEntry], orientation: MenuOrientation = MenuOrientation.Vertical):
        self.options = options
        if orientation == MenuOrientation.Horizontal:
            self.rofi_command = ['rofi', '-dmenu', '-theme', join(CONFIG_DIR, 'theme-bar.rasi'), '-config',
                                 join(CONFIG_DIR, 'config-basic.rasi')]
        else:
            self.rofi_command = ['rofi', '-dmenu', '-theme', join(CONFIG_DIR, 'theme-select.rasi'), '-config',
                                 join(CONFIG_DIR, 'config-basic.rasi')]

    def run(self):
        keybinding_commands = []
        option_text = ''
        for index, entry in enumerate(self.options):
            keybinding_commands += entry.get_parameter(index)
            option_text += entry.get_option_text() + '\n'

        echo = Popen(['echo', '-e', option_text[:-1]], stdout=PIPE)
        print(self.rofi_command + keybinding_commands)
        chosen_option = run(self.rofi_command + keybinding_commands, capture_output=True, stdin=echo.stdout)

        chosen_entry = None
        if chosen_option.returncode != 0 and chosen_option.returncode >= 10 \
                and chosen_option.returncode < len(self.options) + 10:
            chosen_entry = self.options[chosen_option.returncode - 10]
        else:
            chosen_string = chosen_option.stdout.decode(sys.stdout.encoding).rstrip()
            for entry in self.options:
                if entry.get_option_text() == chosen_string:
                    chosen_entry = entry
                    break

        if chosen_entry is not None:
            chosen_entry.next_item.run()


class FPMenu(RPMenu):
    """
    Child of RPMenu, uses fuzzel instead of rofi
    """

    def __init__(self, options: List[RPMenuEntry], orientation: MenuOrientation = MenuOrientation.Vertical):
        self.options = options
        if orientation == MenuOrientation.Horizontal:
            self.rofi_command = ['fuzzel', '-dmenu']
        else:
            self.rofi_command = ['fuzzel', '-dmenu']
