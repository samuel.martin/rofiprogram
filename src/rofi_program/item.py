from subprocess import run
from collections.abc import Callable


class RPItem:
    """ Abstact base class representing a node """

    def run(self):
        pass


class RPCommand(RPItem):
    """ A Leaf node, which executes some command when run """
    command: str
    args: dict

    def __init__(self, command, args={}):
        self.command = command
        self.args = args

    def run(self):
        run(self.command, **self.args)


class RPAction(RPCommand):
    """ A leaf node, which executes the given lambda function when run"""
    action: Callable

    def __init__(self, action: Callable):
        self.action = action

    def run(self):
        self.action()


class RPFunction(RPItem):
    """ A Leaf node, which executes an arbitrary function with arbitrary arguments """

    def __init__(self, function, *args, **kwargs):
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.function(*self.args, **self.kwargs)
