from rofi_program.item import RPAction
from rofi_program.menu import RPMenu, RPMenuEntry
from programs import BluetoothControl, ExampleProgram, MonitorControl, Program


class ListProgram(Program):
    def __init__(self):
        options = [
                RPMenuEntry('Bluetooth (b)', 'b', RPAction(lambda: BluetoothControl().run())),
                RPMenuEntry('Example (e)', 'e', RPAction(lambda: ExampleProgram().run())),
                RPMenuEntry('Monitor (m)', 'm', RPAction(lambda: MonitorControl().run())),
                ]
        self.menu = RPMenu(options)


def main():
    ListProgram().run()


if __name__ == '__main__':
    main()
